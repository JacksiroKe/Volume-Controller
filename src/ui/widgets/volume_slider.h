#ifndef VOLUMESLIDER_H
#define VOLUMESLIDER_H

#include <QWidget>

class AbstractVolume;

class VolumeSlider : public QWidget
{
    class Internal;
    Internal *stuff;
    
public:
    explicit VolumeSlider(QSharedPointer<AbstractVolume> model, QWidget *parent = 0);
    virtual ~VolumeSlider();
    
    QSharedPointer<AbstractVolume> volumeModel();
    void setVolumeModel(QSharedPointer<AbstractVolume> model);
    
public slots:
    void linkChannels(bool linked);
    void hideChannels(bool hidden);
    void setMasterVisible(bool visible);
};

#endif // VOLUMESLIDER_H
