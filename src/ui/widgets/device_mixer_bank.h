#ifndef DEVICEMIXERBANK_H
#define DEVICEMIXERBANK_H

#include <QWidget>
#include <memory>

#include <QAbstractItemModel>

class DeviceMixerBank : public QWidget
{
    Q_OBJECT
    
    class Internal;
    std::unique_ptr<Internal> stuff;
    
public:
    explicit DeviceMixerBank(QAbstractItemModel *devices, QWidget *parent = nullptr);
    
signals:
    
private slots:
    void selectedDeviceChanged_internal(int index);
};

#endif // DEVICEMIXERBANK_H
