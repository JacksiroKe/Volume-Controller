#include <QGridLayout>
#include <QSlider>
#include <QLabel>
#include <QSignalMapper>
#include <QList>

#include <models/abstract_volume.h>
#include "volume_slider_internal.h"
#include "volume_slider.h"

VolumeSlider::VolumeSlider(QSharedPointer<AbstractVolume> model, QWidget *parent) : QWidget(parent) {
    stuff = new Internal(model, this);
}

VolumeSlider::~VolumeSlider() {
    delete stuff;
}

QSharedPointer<AbstractVolume> VolumeSlider::volumeModel() {
    return stuff->model;
}

void VolumeSlider::linkChannels(bool linked) {
    stuff->linkChannels(linked);
}

void VolumeSlider::hideChannels(bool hidden) {
    stuff->hideChannels(hidden);
}

void VolumeSlider::setMasterVisible(bool visible) {
    stuff->setMasterVisible(visible);
}
