VolumeController Utility

Structure:
src 
    - main.cpp  # Genesis of the app
    - ui        # Implementation of VolumeController (Windows version)
        - main_screen.h
        - main_screep.cpp   # has a groupbox with a lineedit, slider and button but very functional
        
    - utils     # VolumeController Class
        - app_list.h                # defines the application list populator: majorly the apps that produce sound
        - app_list.cpp
        - volume_control.h          # wrapper for the VolumeController
        - volume_control_native.h   # The Volume Controller Native Class tapping into Win Api 
        
